import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        rtcPeerConnection: null as null | RTCPeerConnection,
        dataChannel: null as null | RTCDataChannel,
        fileChannel: null as null | RTCDataChannel,
        file2Channel: null as null | RTCDataChannel
    },
    mutations: {
        setRtcPeerConnection(
            state,
            newRTCPeerConnection: RTCPeerConnection | null
        ) {
            state.rtcPeerConnection = newRTCPeerConnection;
        },
        setDataChannel(state, newDataChannel: RTCDataChannel | null) {
            state.dataChannel = newDataChannel;
        },
        setFileChannel(state, newDataChannel: RTCDataChannel | null) {
            state.fileChannel = newDataChannel;
        },
        setFile2Channel(state, newDataChannel: RTCDataChannel | null) {
            state.file2Channel = newDataChannel;
        }
    },
    actions: {},
    modules: {}
});
