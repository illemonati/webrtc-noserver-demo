module.exports = {
    transpileDependencies: ["vuetify"],
    publicPath: process.env.BASE_URL,
    pwa: {
        name: "Webrtc Noserver Demo",
        appleMobileWebAppCapable: "yes",
        appleMobileWebAppStatusBarStyle: "translucent",
        workboxPluginMode: "GenerateSW"
    }
};
